# Author: Lilian BESSON
# Email: lilian DOT besson AT normale D O T fr
# Version: 1
# Date: 28-08-2015
#
# Makefile for cartes folder.

CP = ~/bin/CP --delete --exclude=\.htaccess
GPG = gpg --no-batch --use-agent --detach-sign --armor --quiet --yes

git:
	git add README.md Makefile
	git commit -m "Auto commit made with 'make git'."
	git push

send_zamok:
	cd /home/lilian/publis/ ; $(CP) ./cartes/* besson@zamok.crans.org:~/www/cartes/ ; cd /home/lilian/publis/cartes/

send_dpt:
	cd /home/lilian/publis/ ; $(CP) ./cartes/* lbesson@ssh.dptinfo.ens-cachan.fr:~/www/cartes/ ; cd /home/lilian/publis/cartes/

zip:
	cd /home/lilian/publis/ ; zip -r -9 ~/publis/cartes.zip ./cartes/ ; cd /home/lilian/publis/cartes/
	$(GPG) ~/publis/cartes.zip
	$(CP) ~/publis/cartes.zip* besson@zamok.crans.org:~/www/dl/
	$(CP) ~/publis/cartes.zip* lbesson@ssh.dptinfo.ens-cachan.fr:~/www/dl/
	mv -vf ~/publis/cartes.zip* ~/Dropbox/
