# Cartes interactives
[Ce petit dépôt Git](https://bitbucket.org/lbesson/cartes/) (privé) héberge des cartes interactives, réalisées par [mon frère Florian Besson](http://paris-sorbonne.academia.edu/FBesson), et utilisées dans ses travaux de recherche pour sa thèse (2013-2018).

### Réalisation
Ces cartes sont dessinées sous [Inkspace](http://www.inkscape.org/) et [The GIMP](http://www.gimp.org/).

### Autre endroit
D'autres cartes sont désormais [ici](http://www.normalesup.org/~fbesson/).

----

### Licence
Ce projet est distribué publiquement sous les conditions de la **licence MIT**.
Pour plus de détails, veuillez lire ce fichier [http://lbesson.mit-license.org/](http://lbesson.mit-license.org/).
